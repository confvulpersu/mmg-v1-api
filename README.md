[![Hippocratic License HL3-CORE](https://img.shields.io/static/v1?label=Hippocratic%20License&message=HL3-CORE&labelColor=5e2751&color=bc8c3d)](https://firstdonoharm.dev/version/3/0/core.html)

# Installer ce projet
Ce projet necessite php et composer.
Versions minimales : 
 - php 8.1 (dom curl ext xml ) # sudo apt sudo apt install php8.2-{ext,xml,curl,dom}
 - composer 2.5.5
 - node (sudo apt install npm && sudo npm install n -g && sudo n stable)
 
```shell
git clone <project-url>
cd <project-url>
make
```
# Démarrer le projet
```shell
make up
```

# Structure du projet

Docker:sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
- laravel.test : Notre projet
- mysql : Base de données
- redis #TODO : Installé par défaut voir quand l'intégrer. Pour les queues mais pas que... 
- meilisearch #TODO : Installé par défaut  Voir a quoi ca sert
- mailpit #TODO : Installé par défaut Voir comment ca fonctionne
- selenium #TODO :  Installé par défaut  L'utiliser

Laravel:
- Laravel Jetstream : gestionnaire de paquets front + auth
  - Laravel Breeze (Auth)
  - Laravel Sanctum (API Token)
  - Laravel Inertia (VueJs)
- Laravel Backpack : Generateur de back office
- darkaonline/l5-swagger : Swagger (documentation de l'api via OpenAPI)
<hr/>

Routes: 
 - Api GET + token: http://localhost/api/user
 - Back office : http://localhost/admin
 - Documentation api : http://localhost/api/documentation


## License

TODO
