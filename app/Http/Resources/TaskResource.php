<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'task_definition' => new TaskDefinitionResource($this->whenNotNull($this->task_definition)),
            'zone' => new ZoneResource($this->whenNotNull($this->zone)),
            'plant' => new PlantResource($this->whenNotNull($this->plant)),
        ];
    }
}
