<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskDefinitionResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'category' => $this->category,
            'date_beg' => $this->date_beg,
            'date_end' => $this->date_end,
            'task_definition_type' => new TaskDefinitionTypeResource($this->whenNotNull($this->task_definition_type)),
            'plant_definition' => new PlantDefinitionResource($this->whenNotNull($this->plant_definition)),
        ];
    }
}
